$(document).ready(function($){
    //init custom select
    jcf.customForms.replaceAll();

    initSameHeight();

    $('img[usemap]').rwdImageMaps();

    $("#owl-carousel").owlCarousel({

        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        navigationText : true
    });
    $('#mob-nav-control').click(function(e){
        e.stopPropagation();
        $(this).parent('.wrap-top-menu').toggleClass('active');
    });
    $(document).click(function(e){
        e.stopPropagation();
        $(this).find('.wrap-top-menu').removeClass('active');
    });
    $('.wrap-feedback button').on('click', function(){
        $(document).find('.popup').show();
    });
    $('.btn-close').on('click', function(){
        $(this).parents('.popup').hide();
    });
    $(window).scroll(function(e){
        if($(this).scrollTop() > 0){
            $('.header').addClass('fixed');
            $('.page').addClass('header-fixed');
        }else{
            $('.header').removeClass('fixed');
            $('.page').removeClass('header-fixed');
        }
    });
    $('.read-more-btn.show').click(function(e){
        e.preventDefault();
        $(this).parents('.wrap-show-hide').addClass('show');
        $(this).parents('.wrap-show-hide').find('.in-show-hide').slideDown('slow');
    });
    $('.read-more-btn.hide').click(function(e){
        e.preventDefault();
        $(this).parents('.wrap-show-hide').removeClass('show');
        $(this).parents('.wrap-show-hide').find('.in-show-hide').slideUp('slow');
    });
});
